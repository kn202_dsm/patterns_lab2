﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MyFactory : MonoBehaviour
{
    public GameObject CubPrefab;

    public static MyFactory Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        Instance = this;
    }

    public void SimpleCreate()
    {
        Debug.Log("Factory");
        var gameObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        gameObj.transform.position = new Vector3(0, 10, 0);
        gameObj.GetComponent<MeshRenderer>().material.color = Color.green;
        gameObj.AddComponent<Rigidbody>();
    }

    public void Create(int x, int y, int z)
    {
        Debug.Log("Factory");
        var gameObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        gameObj.transform.position = new Vector3(x, y, z);
        gameObj.GetComponent<MeshRenderer>().material.color = Color.blue;
        gameObj.AddComponent<Rigidbody>();
    }
}
