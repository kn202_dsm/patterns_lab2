﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerControll : MonoBehaviour
{
    public MyFactory factory;

    public void SimpleCreate()
    {
        Debug.Log("Player Controll");
        var gameObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        gameObj.transform.position = new Vector3(0, 10, 0);
        gameObj.GetComponent<MeshRenderer>().material.color = Color.green;
        gameObj.AddComponent<Rigidbody>();
    }

    public void Create(int x, int y, int z)
    {
        Debug.Log("Player Controll");
        var gameObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        gameObj.transform.position = new Vector3(x, y, z);
        gameObj.GetComponent<MeshRenderer>().material.color = Color.blue;
        gameObj.AddComponent<Rigidbody>();
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {       
        if (Input.GetButton("Jump"))
        {
            if (Input.GetButtonDown("Fire1"))
            {
                SimpleCreate();
            }
            if (Input.GetButtonDown("Fire2"))
            {
                Create(5, 10, 5);
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1"))
            {
                MyFactory.Instance.SimpleCreate();
            }
            if (Input.GetButtonDown("Fire2"))
            {
                MyFactory.Instance.Create(5, 10, 5);
            }
        }
    }
}